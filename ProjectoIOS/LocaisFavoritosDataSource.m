//
//  LocaisFavoritosDataSource.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 09/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import "LocaisFavoritosDataSource.h"

@implementation LocaisFavoritosDataSource

+ (instancetype)defaultDataSource {
    
    static LocaisFavoritosDataSource *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        instance = [[LocaisFavoritosDataSource alloc] init];
        
    });
    
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _locaisFavoritosArr = [[NSMutableArray alloc] init];
    }
    return self;
}

@end
