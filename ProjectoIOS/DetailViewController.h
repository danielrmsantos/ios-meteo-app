//
//  DetailViewController.h
//  ProjectoIOS
//
//  Created by Daniel Santos on 08/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController


@property (strong, nonatomic) NSDictionary *forecastDataRecebida;


@end
