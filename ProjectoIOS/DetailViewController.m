//
//  DetailViewController.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 08/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import "DetailViewController.h"
#import "PrevisoesViewController.h"
#import <UIImageView+AFNetworking.h>

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *tempMin;
@property (weak, nonatomic) IBOutlet UILabel *tempMax;
@property (weak, nonatomic) IBOutlet UILabel *tempDay;
@property (weak, nonatomic) IBOutlet UILabel *tempNight;
@property (weak, nonatomic) IBOutlet UILabel *tempEve;
@property (weak, nonatomic) IBOutlet UILabel *tempMorning;
@property (weak, nonatomic) IBOutlet UILabel *pressure;
@property (weak, nonatomic) IBOutlet UILabel *humidity;
@property (weak, nonatomic) IBOutlet UILabel *speed;
//@property (weak, nonatomic) IBOutlet UILabel *deg;
@property (weak, nonatomic) IBOutlet UILabel *clouds;
@property (weak, nonatomic) IBOutlet UILabel *rain;
@property (weak, nonatomic) IBOutlet UILabel *snow;
@property (weak, nonatomic) IBOutlet UILabel *descricao;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItem;
@property (weak, nonatomic) IBOutlet UIView *tempContainer;
@property (weak, nonatomic) IBOutlet UIView *descricaoContainer;
@property (weak, nonatomic) IBOutlet UIView *restanteContainer;


@end

@implementation DetailViewController
{
    NSDateFormatter *dataFormatter;
    
    NSString *direcaoVento;
    float direcaoEmGraus;
    float precipitacao;
    float neve;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dataFormatter = [[NSDateFormatter alloc]init];
    
    self.tempContainer.layer.cornerRadius = 8;
    self.tempContainer.layer.borderWidth = 1;
    self.descricaoContainer.layer.cornerRadius = 8;
    self.descricaoContainer.layer.borderWidth = 1;
    self.restanteContainer.layer.cornerRadius = 8;
    self.restanteContainer.layer.borderWidth = 1;
    
    //PrevisoesViewController *p = [[PrevisoesViewController alloc]init];
    NSLog(@"%@", _forecastDataRecebida);
    
    //mostra temperaturas
    self.tempDay.text = [NSString stringWithFormat:@"%.1fºC",[_forecastDataRecebida[@"temp"][@"day"] floatValue] ];
    self.tempEve.text = [NSString stringWithFormat:@"%.1fºC",[_forecastDataRecebida[@"temp"][@"eve"] floatValue] ];
    self.tempMax.text = [NSString stringWithFormat:@"%.1fºC",[_forecastDataRecebida[@"temp"][@"max"] floatValue] ];
    self.tempMin.text = [NSString stringWithFormat:@"%.1fºC",[_forecastDataRecebida[@"temp"][@"min"] floatValue] ];
    self.tempNight.text = [NSString stringWithFormat:@"%.1fºC",[_forecastDataRecebida[@"temp"][@"night"] floatValue] ];
    self.tempMorning.text = [NSString stringWithFormat:@"%.1fºC",[_forecastDataRecebida[@"temp"][@"morn"] floatValue] ];
    
    //mostra humidade
    self.humidity.text = [NSString stringWithFormat:@"%@%%", _forecastDataRecebida[@"humidity"]];
    
    //mostra pressao
    self.pressure.text = [NSString stringWithFormat:@"%@hPa", _forecastDataRecebida[@"pressure"]];
    
    //self.geral.text = [NSString stringWithFormat:@"%@", _forecastDataRecebida[@"pressure"]];
    
    
    //mostra rain
    self.rain.text = [NSString stringWithFormat:@"%@", _forecastDataRecebida[@"rain"]];
    
    //mostra direcao vento
    direcaoEmGraus = [_forecastDataRecebida[@"deg"]floatValue ];
    [self windDirectionFromDegrees:direcaoEmGraus];
    //self.deg.text = [NSString stringWithFormat:@"%@", direcaoVento];
    //mostra veloc vento
    self.speed.text = [NSString stringWithFormat:@"%@m/s %@", _forecastDataRecebida[@"speed"], direcaoVento];
    
    //mostra nuvens
    self.clouds.text = [NSString stringWithFormat:@"%@%%", _forecastDataRecebida[@"clouds"]];
    
    //mostra precipitacao
    precipitacao = [_forecastDataRecebida[@"rain"] floatValue];
    if (precipitacao == 0.0) {
        precipitacao = 0;
    } else {
        precipitacao = (precipitacao/3);
    }
    self.rain.text = [NSString stringWithFormat:@"%.2fmm/h", precipitacao];
    
    //mostra neve
    neve = [_forecastDataRecebida[@"snow"] floatValue];
    if (neve == 0) {
        neve = 0;
    } else {
        neve = (neve/3);
    }
    self.snow.text = [NSString stringWithFormat:@"%.2fmm/h", neve];
    
    //mostra descricao
    self.descricao.text = [[NSString stringWithFormat:@"%@", _forecastDataRecebida[@"weather"][0][@"description"]]capitalizedString];
    
    //mostra icon
    NSString *iconId = [NSString stringWithFormat:@"%@", _forecastDataRecebida[@"weather"][0][@"icon"]];
    NSString *enderecoApiIcon = @"http://openweathermap.org/img/w/";
    NSMutableString *enderecoFinalIcon = [NSMutableString stringWithFormat:@"%@%@.png", enderecoApiIcon, iconId];
    [_icon setImageWithURL:[NSURL URLWithString:enderecoFinalIcon]];
    
    //mostra data
    NSString *preparaDataParaCelula = [NSString stringWithFormat:@"%@",_forecastDataRecebida[@"dt"] ];
    
    NSArray *dataSeparadaCelula = [preparaDataParaCelula componentsSeparatedByString:@" "];
    
    NSString *dataDoArrayCelula = dataSeparadaCelula[0];
    //NSString *horaDoArrayCelula = dataSeparadaCelula[1];
    //NSLog(@"DATA_>_>_> %@ HORA_>_>_>%@", dataDoArrayCelula, horaDoArrayCelula);
    
    dataFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *dataParaCelula = [dataFormatter dateFromString:dataDoArrayCelula];
    //NSLog(@"DATA PARA CELULA_>_>_> %@", dataParaCelula);
    
    dataFormatter.dateFormat = @"dd";
    NSString *dataDia = [dataFormatter stringFromDate:dataParaCelula];
    
    dataFormatter.dateFormat = @"MMMM";
    NSString *dataMes = [dataFormatter stringFromDate:dataParaCelula];
    
    dataFormatter.dateFormat = @"EEEE";
    NSString *diaCelula = [[dataFormatter stringFromDate:dataParaCelula] capitalizedString];
    
    //NSString *horaFormatada = [horaDoArrayCelula substringToIndex:5];
    NSString *dataHoraCelula = [[NSString stringWithFormat:@"%@, %@ de %@", diaCelula, dataDia, dataMes] capitalizedString];
    self.navigationItem.title = dataHoraCelula;
}

- (void)windDirectionFromDegrees:(float)degrees
{
    static NSArray *directions;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        // Initialize array on first call.
        directions = @[@"N", @"NNE", @"NE", @"ENE", @"E", @"ESE", @"SE", @"SSE",
                       @"S", @"SSO", @"SO", @"OSO", @"O", @"ONO", @"NO", @"NNO"];
    });
    
    int i = (degrees + 11.25)/22.5;
    direcaoVento = [NSString stringWithFormat:@"%@", directions[i % 16]];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
