//
//  CityCreator.h
//  ProjectoIOS
//
//  Created by Daniel Santos on 03/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityCreator : NSObject

+ (void)createCities;

@end
