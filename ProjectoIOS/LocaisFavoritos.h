//
//  LocaisFavoritos.h
//  ProjectoIOS
//
//  Created by Daniel Santos on 09/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocaisFavoritos : NSObject<NSCoding>

@property (strong, nonatomic) NSString *cidade;
@property (strong, nonatomic) NSString *idCidade;
@property (strong, nonatomic) NSString *country;

- (instancetype)initWithNome:(NSString *)cidade idCidade:(NSString *)idCidade country:(NSString *)country;
+ (instancetype)locaisFavoritosWithNome:(NSString *)cidade idCidade:(NSString *)idCidade country:(NSString *)country;

@end
