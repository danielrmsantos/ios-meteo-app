//
//  Singleton.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 08/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

+ (instancetype)sharedInstance {
    
    static Singleton *instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        instance = [[Singleton alloc] init];
        instance.novosLocais = [[NSMutableArray alloc]init];
    });
    
    return instance;
    
}



@end
