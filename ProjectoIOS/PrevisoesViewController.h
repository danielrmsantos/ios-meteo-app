//
//  PrevisoesViewController.h
//  ProjectoIOS
//
//  Created by Daniel Santos on 07/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrevisoesViewController : UIViewController


@property (strong, nonatomic) NSDictionary *forecastData;
@property (strong, nonatomic) NSDictionary *forecastDataAEnviar;

@end
