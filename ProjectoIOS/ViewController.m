//
//  ViewController.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 26/11/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import "ViewController.h"
#import <OWMWeatherAPI.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <CoreLocation/CoreLocation.h>
#import "Reachability.h"
#import <UIImageView+AFNetworking.h>
#import "Singleton.h"
#import "CityCreator.h"
#import "AppDelegate.h"
#import "CityInfo+CoreDataClass.h"

@interface ViewController () <CLLocationManagerDelegate, UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UILabel *cityName;
@property (weak, nonatomic) IBOutlet UILabel *timeStamp;
@property (weak, nonatomic) IBOutlet UILabel *weather;
@property (weak, nonatomic) IBOutlet UILabel *currentTemp;
@property (weak, nonatomic) IBOutlet UIView *minMaxContainer;
@property (weak, nonatomic) IBOutlet UILabel *tempMin;
@property (weak, nonatomic) IBOutlet UILabel *tempMax;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewAoLongoDoDia;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionProx24H;
@property (weak, nonatomic) IBOutlet UILabel *humidadeLabel;
@property (weak, nonatomic) IBOutlet UILabel *velocVentoLabel;
@property (weak, nonatomic) IBOutlet UILabel *horaNascerSolLabel;
@property (weak, nonatomic) IBOutlet UILabel *horaPorSolLabel;
@property (strong, nonatomic) NSString *mensagemNotification;
@property (strong, nonatomic) NSString *cityId;
@property (strong, nonatomic) NSString *cidadePesquisaLivre;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSArray<CityInfo *> *cidadesLista;



@end

@implementation ViewController
{
    OWMWeatherAPI *_weatherAPI;
    NSArray *_forecast;
    
    NSDateFormatter *_dateFormatter;
    NSDateFormatter *dataFormatter;
    
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    CLLocationCoordinate2D coordinate;

    int downloadCount;
    NSNumber *idPrevisao;
    NSString *idPrevisaoSingleton;
    BOOL arranque;
    BOOL veioDaPesquisa;
    BOOL veioDaPesquisaLivre;
    NSString *apiKey;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    apiKey = @"PUT YOUR OPEN WEATHER MAP API KEY HERE";
    
    [self testaLigacaoNet];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incomingNotification:) name:@"teste"object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incomingNotificationId:) name:@"idCidade"object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incomingNotificationidCidadeFavorita:) name:@"CidadeFavorita"object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incomingNotificationPesquisaLivre:) name:@"pesquisaLivre"object:nil];
    
    [CityCreator createCities];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    NSFetchRequest *fr = [CityInfo fetchRequest];
    
    self.cidadesLista = [context executeFetchRequest:fr error:nil];
    
    NSLog(@"A MINHA LISTA CIDADES %@", self.cidadesLista[2].name);
    
    _dateFormatter = [[NSDateFormatter alloc]init];
    dataFormatter = [[NSDateFormatter alloc]init];
    self.locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    
    self.locationManager.delegate = self;
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    self.minMaxContainer.layer.cornerRadius = 8;
    self.minMaxContainer.layer.borderWidth = 1;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [self.locationManager startUpdatingLocation];
    
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        NSLog(@"GPS ACTIVO");
    } else {
        NSLog(@"GPS DESLIGADO");
    }

}

- (void)viewWillAppear:(BOOL)animated {
    if (!_cityId && arranque) {
        //[self mostraDadosGPSnoArranque];
        NSLog(@"___________________CITY ID COM MENSAGEM: %@________________as_", _cityId);
    } else {
        //[self mostraDadosDaPesquisaLivre];
        
        NSLog(@"___________________CITY ID PRENCHIDO________________as_");
    }
    
    //[self mostraDados];
    //NSLog(@"%@", )
}

-(void)viewDidAppear:(BOOL)animated{
    if (!arranque) {
        NSLog(@"ESTOU A ARRANCAR");
        [self alertaInicial];
    }
    
    if(veioDaPesquisa){
        [self mostraDadosDaPesquisa];
        
        NSLog(@"VIM DA PESQUISA POR ID/LISTA");
    }
    
    if(veioDaPesquisaLivre){
        [self mostraDadosDaPesquisaLivre];
       
        NSLog(@"VIM DA PESQUISA LIVRE");
    }else{
        NSLog(@"AINDA NAO SEI");
    }
    
//    NSLog(@"____ID PREVISAO SIGLETON:%@______", idPrevisaoSingleton);
//    NSLog(@"_________VEIO DA PESQUISA:%hhd", veioDaPesquisa);
//    NSLog(@"_________VEIO DA PESQUISA LIVRE:%hhd", veioDaPesquisaLivre);
}

-(void)viewWillDisappear:(BOOL)animated{
  //  [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[[NSNotificationCenter defaultCenter] postNotificationName: @"idPrevisao" object:idPrevisao];
    //NSLog(@"___ID NOTIF VIEWWILLDISAPEAR:%@________", idPrevisao);
    
    veioDaPesquisa = NO;
    veioDaPesquisaLivre = NO;
//    NSLog(@"_________VEIO DA PESQUISA:%hhd", veioDaPesquisa);
//    NSLog(@"_________VEIO DA PESQUISA LIVRE:%hhd", veioDaPesquisaLivre);
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) incomingNotificationidCidadeFavorita:(NSNotification *)notification{
    NSString *theString = [notification object];
    veioDaPesquisa = YES;
    //NSLog(@"_________NOTIFICAÇÂO Do ID FAVORITO:%@",theString);
    _cidadePesquisaLivre = theString;
    if (_cidadePesquisaLivre) {
    [self mostraDadosDaPesquisaLivre];
    }
    

}

- (void) incomingNotificationId:(NSNotification *)notification{
    NSString *theString = [notification object];
    veioDaPesquisa = YES;
    //NSLog(@"_________NOTIFICAÇÂO Do ID:%@",theString);
    _cityId = theString;
    [self mostraDadosDaPesquisa];
    
}

- (void) incomingNotificationPesquisaLivre:(NSNotification *)notification{
    NSString *theString = [notification object];
    veioDaPesquisaLivre = YES;
    //NSLog(@"_________NOTIFICAÇÂO Da PESQUISA LIVRE:%@",theString);
    _cidadePesquisaLivre = theString;
    [self mostraDadosDaPesquisaLivre];
    
}

- (void) incomingNotification:(NSNotification *)notification{
    NSString *theString = [notification object];
    //NSLog(@"_________NOTIFICAÇÂO:%@",theString);
    _mensagemNotification = theString;
    
    [self mostraDados];
}

// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    NSLog(@"AS minha coord %@", [locations lastObject]);
    
    //CLLocation *minhaLocation = locations[0];
    [self.locationManager stopUpdatingLocation];
    CLLocation *location = [_locationManager location];
    //NSLog(@"LOCATION:%@", location);
    coordinate = [location coordinate];

}

-(void)mostraDados {
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
    
    NSDate *dateToday = [NSDate date];
    
    NSString *modeloData = @"H:m yyyyMMdd";
    NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:modeloData options:0 locale:[NSLocale systemLocale] ];
    
    [_dateFormatter setDateFormat:dateFormat];
    _forecast = @[];
    _weatherAPI = [[OWMWeatherAPI alloc]initWithAPIKey:apiKey];
    
    [_weatherAPI setLangWithPreferedLanguage];
    [_weatherAPI setTemperatureFormat:kOWMTempCelcius];

      [_weatherAPI dailyForecastWeatherByCoordinate:coordinate withCount:8 andCallback:^(NSError *error, NSDictionary *result) {
          downloadCount++;
          if (downloadCount > 1) {
        
          }
          
          if (error) {
              // Handle the error
              if (error.code == -1009) {
                  NSLog(@"______NAO TENS NET___________");
                  [self alertaFalhaNET];
              }
              if (error.code == -1011) {
                  NSLog(@"______NAO ENCONTROU A CIDADE___________");
                  [self alertaCidadeNaoEncontrada];
              }
              
              //NSLog(@"--ERRO POR COORDENADAS dailyForecastWeatherByCoordinate MOSTRADADOS:%@, CODIGO ERRO:%d", error, error.code);
              return;
          }
        
        }];
    
    [_weatherAPI currentWeatherByCoordinate:coordinate withCallback:^(NSError *error, NSDictionary *result) {
        downloadCount++;
        if (downloadCount > 1) {
           
            
        }
        
        
        if (error) {
            // Handle the error
            if (error.code == -1009) {
                //NSLog(@"______NAO TENS NET___________");
                [self alertaFalhaNET];
            }
            if (error.code == -1011) {
                NSLog(@"______NAO ENCONTROU A CIDADE___________");
                [self alertaCidadeNaoEncontrada];
            }
            //NSLog(@"--ERRO POR COORDENADAS currentWeatherByCoordinate--%@, CODIGO ERRO:%d", error, error.code);
            return;
        }
        
        // mostra cidade
                  self.cityName.text = [NSString stringWithFormat:@"%@, %@",
                                        result[@"name"],
                                        result[@"sys"][@"country"]
                                        ];
        
    }];

    [_weatherAPI currentWeatherByCityId:_cityId withCallback:^(NSError *error, NSDictionary *result) {
        if (!_cityId) {
            _cityId = _mensagemNotification;
            NSLog(@"___________________CITY ID COM MENSAGEM: %@________________as_", _cityId);
        } else {
            NSLog(@"___________________CITY ID PRENCHIDO________________as_");
        }

        NSLog(@"MENSAGEM _>_>_>_>_>_>>_>_>%@", _mensagemNotification);
        downloadCount++;
        if (downloadCount > 1) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
        
        if (error) {
            // Handle the error
            if (error.code == -1009) {
                NSLog(@"______NAO TENS NET___________");
                [self alertaFalhaNET];
            }
            if (error.code == -1011) {
                NSLog(@"______NAO ENCONTROU A CIDADE___________");
                [self alertaCidadeNaoEncontrada];
            }
            //NSLog(@"--ERRO CITY ID--%@ ---CODIGO ERRO:%d", error, error.code);
            return;
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:result forKey:@"ultimaCurrent"];
        [defaults synchronize];
        
        idPrevisaoSingleton = result[@"id"];
        
        Singleton *single = [Singleton sharedInstance];
        single.idCidade = idPrevisaoSingleton;
        
        // mostra cidade
        self.cityName.text = [NSString stringWithFormat:@"%@, %@",
                              result[@"name"],
                              result[@"sys"][@"country"]
                              ];
        
        // mostra temp actual
        self.currentTemp.text = [NSString stringWithFormat:@"%.1fºC",
                                 [result[@"main"][@"temp"] floatValue] ];
        
        // mostra humidade
        self.humidadeLabel.text = [NSString stringWithFormat:@"%@%%",
                                   result[@"main"][@"humidity"]];
        
        // mostra velocidade vento
        self.velocVentoLabel.text = [NSString stringWithFormat:@"%@m/s",
                                     result[@"wind"][@"speed"]];
        
        // mostra nascer sol
        NSString *horaNascerSol = [NSString stringWithFormat:@"%@",result[@"sys"][@"sunrise"]];
        NSRange rangeHora = NSMakeRange(11, 5);
        NSString *horaNascerSolFormatada = [horaNascerSol substringWithRange:rangeHora];
        self.horaNascerSolLabel.text = horaNascerSolFormatada;
        
        // mostra por sol
        NSString *horaPorSol = [NSString stringWithFormat:@"%@",result[@"sys"][@"sunset"]];
        NSString *horaPorSolFormatada = [horaPorSol substringWithRange:rangeHora];
        
        //NSLog(@"_____HORA POR SOL: %@________",horaPorSol);
        //NSLog(@"_____HORA POR SOL FORMATADA:%@________",horaPorSolFormatada);
        
        self.horaPorSolLabel.text = horaPorSolFormatada;
        
        // mostra temp min
        self.tempMin.text = [NSString stringWithFormat:@"%.1fºC",
                             [result[@"main"][@"temp_min"] floatValue] ];
        
        // mostra temp max
        self.tempMax.text = [NSString stringWithFormat:@"%.1fºC",
                             [result[@"main"][@"temp_max"] floatValue] ];
        
        // mostra ultima actalização
        //x(@"____________________-%@", result[@"dt"]);
        NSString *dataArray = [_dateFormatter stringFromDate:result[@"dt"]];
        NSString *dataArrayFormatada = [dataArray substringToIndex:11];
        NSString *horaArrayFormatada = [dataArray substringFromIndex:11];
        
        //NSLog(@"_____HORA DO ARRAY FORMATADA: %@________",horaArrayFormatada);
        //        NSLog(@"_____DATA DO ARRAY: %@________",dataArray);
        //        NSLog(@"_____DATA DO ARRAY FORMATADA: %@________",dataArrayFormatada);
        
        NSString *dataDispositivo = [_dateFormatter stringFromDate:dateToday];
        NSString *dataDispositivoFormatada = [dataDispositivo substringToIndex:11];
        
        //        NSLog(@"_____DATA DO DISPOSITIVO: %@________",dataDispositivo);
        //        NSLog(@"_____DATA DO DISPOSITIVO FORMATADA: %@________",dataDispositivoFormatada);
        
        if ([dataDispositivoFormatada isEqualToString:dataArrayFormatada]) {
            //            NSLog(@"_____PUTA QUE PARIU E IGUAL:________");
            NSString *mostraData = [NSString stringWithFormat:@"Hoje ás %@",horaArrayFormatada];
            self.timeStamp.text =  mostraData;
        } else {
            //            NSLog(@"_____FODASSE NAO E IGUAL:________");
            self.timeStamp.text =  [_dateFormatter stringFromDate:result[@"dt"]];
        }
        
        // mostra condições weather
        self.weather.text = [[NSString stringWithFormat:@"%@",result[@"weather"][0][@"description"]]capitalizedString ];
    
        // Mostra icon AFNETWORKING
        NSString *icon = [NSString stringWithFormat:@"%@", result[@"weather"][0][@"icon"]];
        
        
        NSString *enderecoApiIcon = @"http://openweathermap.org/img/w/";
        NSMutableString *enderecoFinalIcon = [NSMutableString stringWithFormat:@"%@%@.png", enderecoApiIcon, icon];
        
        [_iconImageView setImageWithURL:[NSURL URLWithString:enderecoFinalIcon]];
        
    }];
    
    [_weatherAPI forecastWeatherByCityId:_cityId withCallback:^(NSError *error, NSDictionary *result) {
        downloadCount++;
        if (downloadCount > 1) {
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }
        if (error) {
            // Handle the error;
            if (error.code == -1009) {
                NSLog(@"______NAO TENS NET___________");
                [self alertaFalhaNET];
            }
            if (error.code == -1011) {
                NSLog(@"______NAO ENCONTROU A CIDADE___________");
                [self alertaCidadeNaoEncontrada];
            }
            return;
        }
        
        _forecast = result[@"list"];
        
        [self.collectionProx24H reloadData];
    }];
    
}

-(void)mostraDadosDaPesquisa {
    
    NSDate *dateToday = [NSDate date];
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
    
    NSString *modeloData = @"H:m yyyyMMdd";
    NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:modeloData options:0 locale:[NSLocale systemLocale] ];
    
    [_dateFormatter setDateFormat:dateFormat];
    _forecast = @[];
    _weatherAPI = [[OWMWeatherAPI alloc]initWithAPIKey:apiKey];
    
    [_weatherAPI setLangWithPreferedLanguage];
    [_weatherAPI setTemperatureFormat:kOWMTempCelcius];
    
    [_weatherAPI dailyForecastWeatherByCoordinate:coordinate withCount:8 andCallback:^(NSError *error, NSDictionary *result) {
        downloadCount++;
        if (downloadCount > 1) {
            
        }

        if (error) {
            // Handle the error
            if (error.code == -1009) {
                //NSLog(@"______NAO TENS NET___________");
                [self alertaFalhaNET];
            }
            if (error.code == -1011) {
                //NSLog(@"______NAO ENCONTROU A CIDADE___________");
                [self alertaCidadeNaoEncontrada];
            }
            
            //NSLog(@"--ERRO POR COORDENADAS dailyForecastWeatherByCoordinate MOSTRA DADOS PESQUISA--%@, CODIGO ERRO:%d", error, error.code);
            return;
        }
       
    }];
    

    
    [_weatherAPI currentWeatherByCityId:_cityId withCallback:^(NSError *error, NSDictionary *result) {
        if (!_cityId) {
            _cityId = _mensagemNotification;
            NSLog(@"___________________CITY ID COM MENSAGEM: %@________________as_", _cityId);
        } else {
            NSLog(@"___________________CITY ID PRENCHIDO________________as_");
        }
        
        NSLog(@"MENSAGEM _>_>_>_>_>_>>_>_>%@", _mensagemNotification);
        downloadCount++;
        if (downloadCount > 1) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        
        if (error) {
            // Handle the error
            if (error.code == -1009) {
                NSLog(@"______NAO TENS NET___________");
                [self alertaFalhaNET];
            }
            if (error.code == -1011) {
                NSLog(@"______NAO ENCONTROU A CIDADE___________");
                [self alertaCidadeNaoEncontrada];
            }
            NSLog(@"--ERRO currentWeatherByCityId MOSTRA DADOS PESQUISA--%@, CODIGO ERRO:%ld", error, (long)error.code);
            return;
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:result forKey:@"ultimaCurrent"];
        [defaults synchronize];
        
        idPrevisao = result[@"id"];
        _idTESTE = result[@"id"];
        NSLog(@"_____ID PREVISAO:%@_____", idPrevisao);
        
        idPrevisaoSingleton = result[@"id"];
        
        Singleton *single = [Singleton sharedInstance];
        single.idCidade = idPrevisaoSingleton;
        
        // mostra cidade
        self.cityName.text = [NSString stringWithFormat:@"%@, %@",
                              result[@"name"],
                              result[@"sys"][@"country"]
                              ];
        //NSLog(@"RESULTADO :%@", result);
        // mostra temp actual
        self.currentTemp.text = [NSString stringWithFormat:@"%.1fºC",
                                 [result[@"main"][@"temp"] floatValue] ];
        
        // mostra humidade
        self.humidadeLabel.text = [NSString stringWithFormat:@"%@%%",
                                   result[@"main"][@"humidity"]];
        
        // mostra velocidade vento
        self.velocVentoLabel.text = [NSString stringWithFormat:@"%@m/s",
                                     result[@"wind"][@"speed"]];
        
        // mostra nascer sol
        NSString *horaNascerSol = [NSString stringWithFormat:@"%@",result[@"sys"][@"sunrise"]];
        NSRange rangeHora = NSMakeRange(11, 5);
        NSString *horaNascerSolFormatada = [horaNascerSol substringWithRange:rangeHora];
        
        //NSLog(@"_____HORA NASCER SOL: %@________",horaNascerSol);
        //NSLog(@"_____HORA NASCER SOL FORMATADA:%@________",horaNascerSolFormatada);
        
        self.horaNascerSolLabel.text = horaNascerSolFormatada;
        
        // mostra por sol
        NSString *horaPorSol = [NSString stringWithFormat:@"%@",result[@"sys"][@"sunset"]];
        NSString *horaPorSolFormatada = [horaPorSol substringWithRange:rangeHora];
        
        //NSLog(@"_____HORA POR SOL: %@________",horaPorSol);
        //NSLog(@"_____HORA POR SOL FORMATADA:%@________",horaPorSolFormatada);
        
        self.horaPorSolLabel.text = horaPorSolFormatada;
        
        // mostra temp min
        self.tempMin.text = [NSString stringWithFormat:@"%.1fºC",
                             [result[@"main"][@"temp_min"] floatValue] ];
        
        // mostra temp max
        self.tempMax.text = [NSString stringWithFormat:@"%.1fºC",
                             [result[@"main"][@"temp_max"] floatValue] ];
        
        // mostra ultima actalização
        //x(@"____________________-%@", result[@"dt"]);
        NSString *dataArray = [_dateFormatter stringFromDate:result[@"dt"]];
        NSString *dataArrayFormatada = [dataArray substringToIndex:11];
        NSString *horaArrayFormatada = [dataArray substringFromIndex:11];
        
        //NSLog(@"_____HORA DO ARRAY FORMATADA: %@________",horaArrayFormatada);
        //        NSLog(@"_____DATA DO ARRAY: %@________",dataArray);
        //        NSLog(@"_____DATA DO ARRAY FORMATADA: %@________",dataArrayFormatada);
        
        NSString *dataDispositivo = [_dateFormatter stringFromDate:dateToday];
        NSString *dataDispositivoFormatada = [dataDispositivo substringToIndex:11];
        
        //        NSLog(@"_____DATA DO DISPOSITIVO: %@________",dataDispositivo);
        //        NSLog(@"_____DATA DO DISPOSITIVO FORMATADA: %@________",dataDispositivoFormatada);
        
        if ([dataDispositivoFormatada isEqualToString:dataArrayFormatada]) {
            //            NSLog(@"_____PUTA QUE PARIU E IGUAL:________");
            NSString *mostraData = [NSString stringWithFormat:@"Hoje ás %@",horaArrayFormatada];
            self.timeStamp.text =  mostraData;
            //self.timeStamp.text =  [_dateFormatter stringFromDate:result[@"dt"]];
        } else {
            //            NSLog(@"_____FODASSE NAO E IGUAL:________");
            self.timeStamp.text =  [_dateFormatter stringFromDate:result[@"dt"]];
        }
        
        // mostra condições weather
        self.weather.text = [[NSString stringWithFormat:@"%@",result[@"weather"][0][@"description"]]capitalizedString ];
        
        
        // Mostra icon AFNETWORKING
        NSString *icon = [NSString stringWithFormat:@"%@", result[@"weather"][0][@"icon"]];
        
        
        NSString *enderecoApiIcon = @"http://openweathermap.org/img/w/";
        NSMutableString *enderecoFinalIcon = [NSMutableString stringWithFormat:@"%@%@.png", enderecoApiIcon, icon];
        
        [_iconImageView setImageWithURL:[NSURL URLWithString:enderecoFinalIcon]];
        
    }];
    
    [_weatherAPI forecastWeatherByCityId:_cityId withCallback:^(NSError *error, NSDictionary *result) {
        downloadCount++;
        if (downloadCount > 1) {
           
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"ola teste ........................");
        }
        if (error) {
            // Handle the error;
            if (error.code == -1009) {
                NSLog(@"______NAO TENS NET___________");
                [self alertaFalhaNET];
            }
            if (error.code == -1011) {
                NSLog(@"______NAO ENCONTROU A CIDADE___________");
                [self alertaCidadeNaoEncontrada];
            }
            return;
        }
        
        _forecast = result[@"list"];
        
        [self.collectionProx24H reloadData];
    }];
    
}

-(void)mostraDadosDaPesquisaLivre {
    
    NSDate *dateToday = [NSDate date];
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];
    
    NSString *modeloData = @"H:m yyyyMMdd";
    NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:modeloData options:0 locale:[NSLocale systemLocale] ];
    
    [_dateFormatter setDateFormat:dateFormat];
    _forecast = @[];
    _weatherAPI = [[OWMWeatherAPI alloc]initWithAPIKey:apiKey];
    
    [_weatherAPI setLangWithPreferedLanguage];
    [_weatherAPI setTemperatureFormat:kOWMTempCelcius];
    
    [_weatherAPI currentWeatherByCityName:_cidadePesquisaLivre withCallback:^(NSError *error, NSDictionary *result) {

        downloadCount++;
        if (downloadCount > 1) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    
        
        if (error) {
            // Handle the error
            if (error.code == -1009) {
                NSLog(@"______NAO TENS NET___________");
                [self alertaFalhaNET];
            }
            if (error.code == -1011) {
                NSLog(@"______NAO ENCONTROU A CIDADE___________");
                [self alertaCidadeNaoEncontrada];
            }

            NSLog(@"--ERRO currentWeatherByCityName--%@, CODIGO ERRO:%ld", error, (long)error.code);
            return;
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:result forKey:@"ultimaCurrent"];
        [defaults synchronize];
        
        idPrevisao = result[@"id"];
        
        idPrevisaoSingleton = result[@"id"];
        
        Singleton *single = [Singleton sharedInstance];
        single.idCidade = idPrevisaoSingleton;
        
        // mostra cidade
        self.cityName.text = [NSString stringWithFormat:@"%@, %@",
                              result[@"name"],
                              result[@"sys"][@"country"]
                              ];
        //NSLog(@"RESULTADO :%@", result);
        // mostra temp actual
        self.currentTemp.text = [NSString stringWithFormat:@"%.1fºC",
                                 [result[@"main"][@"temp"] floatValue] ];
        
        // mostra humidade
        self.humidadeLabel.text = [NSString stringWithFormat:@"%@%%",
                                   result[@"main"][@"humidity"]];
        
        // mostra velocidade vento
        self.velocVentoLabel.text = [NSString stringWithFormat:@"%@m/s",
                                     result[@"wind"][@"speed"]];
        
        // mostra nascer sol
        NSString *horaNascerSol = [NSString stringWithFormat:@"%@",result[@"sys"][@"sunrise"]];
        NSRange rangeHora = NSMakeRange(11, 5);
        NSString *horaNascerSolFormatada = [horaNascerSol substringWithRange:rangeHora];
        
        //NSLog(@"_____HORA NASCER SOL: %@________",horaNascerSol);
        //NSLog(@"_____HORA NASCER SOL FORMATADA:%@________",horaNascerSolFormatada);
        
        self.horaNascerSolLabel.text = horaNascerSolFormatada;
        
        // mostra por sol
        NSString *horaPorSol = [NSString stringWithFormat:@"%@",result[@"sys"][@"sunset"]];
        NSString *horaPorSolFormatada = [horaPorSol substringWithRange:rangeHora];
        
        //NSLog(@"_____HORA POR SOL: %@________",horaPorSol);
        //NSLog(@"_____HORA POR SOL FORMATADA:%@________",horaPorSolFormatada);
        
        self.horaPorSolLabel.text = horaPorSolFormatada;
        
        // mostra temp min
        self.tempMin.text = [NSString stringWithFormat:@"%.1fºC",
                             [result[@"main"][@"temp_min"] floatValue] ];
        
        // mostra temp max
        self.tempMax.text = [NSString stringWithFormat:@"%.1fºC",
                             [result[@"main"][@"temp_max"] floatValue] ];
        
        // mostra ultima actalização
        //x(@"____________________-%@", result[@"dt"]);
        NSString *dataArray = [_dateFormatter stringFromDate:result[@"dt"]];
        NSString *dataArrayFormatada = [dataArray substringToIndex:11];
        NSString *horaArrayFormatada = [dataArray substringFromIndex:11];
        
        //NSLog(@"_____HORA DO ARRAY FORMATADA: %@________",horaArrayFormatada);
        //        NSLog(@"_____DATA DO ARRAY: %@________",dataArray);
        //        NSLog(@"_____DATA DO ARRAY FORMATADA: %@________",dataArrayFormatada);
        
        NSString *dataDispositivo = [_dateFormatter stringFromDate:dateToday];
        NSString *dataDispositivoFormatada = [dataDispositivo substringToIndex:11];
        
        //        NSLog(@"_____DATA DO DISPOSITIVO: %@________",dataDispositivo);
        //        NSLog(@"_____DATA DO DISPOSITIVO FORMATADA: %@________",dataDispositivoFormatada);
        
        if ([dataDispositivoFormatada isEqualToString:dataArrayFormatada]) {
            //            NSLog(@"_____PUTA QUE PARIU E IGUAL:________");
            NSString *mostraData = [NSString stringWithFormat:@"Hoje ás %@",horaArrayFormatada];
            self.timeStamp.text =  mostraData;
            //self.timeStamp.text =  [_dateFormatter stringFromDate:result[@"dt"]];
        } else {
            //            NSLog(@"_____FODASSE NAO E IGUAL:________");
            self.timeStamp.text =  [_dateFormatter stringFromDate:result[@"dt"]];
        }
        
        // mostra condições weather
        self.weather.text = [[NSString stringWithFormat:@"%@",result[@"weather"][0][@"description"]]capitalizedString ];
        NSString *icon = [NSString stringWithFormat:@"%@", result[@"weather"][0][@"icon"]];
        //NSLog(@"%@", icon);
        
       
        // mostra icon AFNETWORKING
        NSString *enderecoApiIcon = @"http://openweathermap.org/img/w/";
        NSMutableString *enderecoFinalIcon = [NSMutableString stringWithFormat:@"%@%@.png", enderecoApiIcon, icon];
        
        [_iconImageView setImageWithURL:[NSURL URLWithString:enderecoFinalIcon]];

    }];
    
    [_weatherAPI forecastWeatherByCityName:_cidadePesquisaLivre withCallback:^(NSError *error, NSDictionary *result) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        
        downloadCount++;
        if (downloadCount > 1) {
            //[self.activityIndicator stopAnimating];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            //NSLog(@"ola teste ........................");
        }
        if (error) {
            // HELDER
            _forecast = [defaults objectForKey:@"forecast"];
            if (!_forecast) {
                
                // FOI A PRIMEIRA VEZ E NUNCA TINHA TIDO NET...
            
            }
            // Handle the error;
            if (error.code == -1009) {
                NSLog(@"______NAO TENS NET___________");
                [self alertaFalhaNET];
            }
            if (error.code == -1011) {
                NSLog(@"______NAO ENCONTROU A CIDADE___________");
                [self alertaCidadeNaoEncontrada];
            }
            NSLog(@"--ERRO POR forecastWeatherByCityName mostraDadosDaPesquisaLivre--%@, CODIGO ERRO:%ldd", error,(long) error.code);
            return;
        }
        
        _forecast = result[@"list"];
        
        [defaults setObject:_forecast forKey:@"forecast"];
        
        [defaults synchronize];
        
        [self.collectionProx24H reloadData];
    }];
    
}



- (void)mostraDadosGPSnoArranque {
NSDate *dateToday = [NSDate date];
    
[[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:YES];

NSString *modeloData = @"H:m yyyyMMdd";
NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:modeloData options:0 locale:[NSLocale systemLocale] ];

    [_dateFormatter setDateFormat:dateFormat];
_forecast = @[];
_weatherAPI = [[OWMWeatherAPI alloc]initWithAPIKey:apiKey];

[_weatherAPI setLangWithPreferedLanguage];
[_weatherAPI setTemperatureFormat:kOWMTempCelcius];

[_weatherAPI dailyForecastWeatherByCoordinate:coordinate withCount:8 andCallback:^(NSError *error, NSDictionary *result) {
    downloadCount++;
    if (downloadCount > 1) {
        
    }
    
    if (error) {
        // Handle the error
        if (error.code == -1009) {
            NSLog(@"______NAO TENS NET___________");
            [self alertaFalhaNET];
        }
        if (error.code == -1011) {
            NSLog(@"______NAO ENCONTROU A CIDADE___________");
            [self alertaCidadeNaoEncontrada];
        }
        NSLog(@"--ERRO POR COORDENADAS dailyForecastWeatherByCoordinate mostraDadosGPSnoArranque--%@", error);
        return;
    }

}];

[_weatherAPI currentWeatherByCoordinate:coordinate withCallback:^(NSError *error, NSDictionary *result) {
    downloadCount++;
    if (downloadCount > 1) {
        
        
    }
    
    if (error) {
        // Handle the error
        if (error.code == -1009) {
            NSLog(@"______NAO TENS NET___________");
            [self alertaFalhaNET];
        }
        if (error.code == -1011) {
            NSLog(@"______NAO ENCONTROU A CIDADE___________");
            [self alertaCidadeNaoEncontrada];
        }
        NSLog(@"--ERRO POR COORDENADAS currentWeatherByCoordinate--%@, CODIGO ERRO:%ld", error, (long)error.code);
        return;
    }
    
    idPrevisao = result[@"id"];
    idPrevisaoSingleton = result[@"id"];
    
    Singleton *single = [Singleton sharedInstance];
    single.idCidade = idPrevisaoSingleton;
    
    // mostra cidade
    self.cityName.text = [NSString stringWithFormat:@"%@, %@",
                          result[@"name"],
                          result[@"sys"][@"country"]
                          ];
    
    //NSLog(@"RESULTADO :%@", result);
    // mostra temp actual
    self.currentTemp.text = [NSString stringWithFormat:@"%.1fºC",
                             [result[@"main"][@"temp"] floatValue] ];
    
    // mostra humidade
    self.humidadeLabel.text = [NSString stringWithFormat:@"%@%%",
                               result[@"main"][@"humidity"]];
    
    // mostra velocidade vento
    self.velocVentoLabel.text = [NSString stringWithFormat:@"%@m/s",
                                 result[@"wind"][@"speed"]];
    
    // mostra nascer sol
    NSString *horaNascerSol = [NSString stringWithFormat:@"%@",result[@"sys"][@"sunrise"]];
    NSRange rangeHora = NSMakeRange(11, 5);
    NSString *horaNascerSolFormatada = [horaNascerSol substringWithRange:rangeHora];
    
    //NSLog(@"_____HORA NASCER SOL: %@________",horaNascerSol);
    //NSLog(@"_____HORA NASCER SOL FORMATADA:%@________",horaNascerSolFormatada);
    
    self.horaNascerSolLabel.text = horaNascerSolFormatada;
    
    // mostra por sol
    NSString *horaPorSol = [NSString stringWithFormat:@"%@",result[@"sys"][@"sunset"]];
    NSString *horaPorSolFormatada = [horaPorSol substringWithRange:rangeHora];
    
    //NSLog(@"_____HORA POR SOL: %@________",horaPorSol);
    //NSLog(@"_____HORA POR SOL FORMATADA:%@________",horaPorSolFormatada);
    
    self.horaPorSolLabel.text = horaPorSolFormatada;
    
    // mostra temp min
    self.tempMin.text = [NSString stringWithFormat:@"%.1fºC",
                         [result[@"main"][@"temp_min"] floatValue] ];
    
    // mostra temp max
    self.tempMax.text = [NSString stringWithFormat:@"%.1fºC",
                         [result[@"main"][@"temp_max"] floatValue] ];
    
    
    
    
    // mostra ultima actalização
    NSString *dataArray = [_dateFormatter stringFromDate:result[@"dt"]];
    NSString *dataArrayFormatada = [dataArray substringToIndex:11];
    NSString *horaArrayFormatada = [dataArray substringFromIndex:11];
    
    //NSLog(@"_____HORA DO ARRAY FORMATADA: %@________",horaArrayFormatada);
    //        NSLog(@"_____DATA DO ARRAY: %@________",dataArray);
    //        NSLog(@"_____DATA DO ARRAY FORMATADA: %@________",dataArrayFormatada);
    
    NSString *dataDispositivo = [_dateFormatter stringFromDate:dateToday];
    NSString *dataDispositivoFormatada = [dataDispositivo substringToIndex:11];
    
    //        NSLog(@"_____DATA DO DISPOSITIVO: %@________",dataDispositivo);
    //        NSLog(@"_____DATA DO DISPOSITIVO FORMATADA: %@________",dataDispositivoFormatada);
    
    if ([dataDispositivoFormatada isEqualToString:dataArrayFormatada]) {
        //            NSLog(@"_____PUTA QUE PARIU E IGUAL:________");
        NSString *mostraData = [NSString stringWithFormat:@"Hoje ás %@",horaArrayFormatada];
        self.timeStamp.text =  mostraData;
        //self.timeStamp.text =  [_dateFormatter stringFromDate:result[@"dt"]];
    } else {
        //            NSLog(@"_____FODASSE NAO E IGUAL:________");
        self.timeStamp.text =  [_dateFormatter stringFromDate:result[@"dt"]];
    }
    
    // mostra condições weather
    self.weather.text = [[NSString stringWithFormat:@"%@",result[@"weather"][0][@"description"]]capitalizedString ];
    
    
    // mostra icon AFNETWORKING
    NSString *icon = [NSString stringWithFormat:@"%@", result[@"weather"][0][@"icon"]];
    NSString *enderecoApiIcon = @"http://openweathermap.org/img/w/";
    NSMutableString *enderecoFinalIcon = [NSMutableString stringWithFormat:@"%@%@.png", enderecoApiIcon, icon];
    
    [_iconImageView setImageWithURL:[NSURL URLWithString:enderecoFinalIcon]];
    
}];

// Previsão para Collection View
[_weatherAPI forecastWeatherByCoordinate:coordinate withCallback:^(NSError *error, NSDictionary *result) {
    downloadCount++;
    if (downloadCount > 1) {
        //[self.activityIndicator stopAnimating];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSLog(@"ola teste ........................");
    }
    if (error) {
        // Handle the error;
        if (error.code == -1009) {
            NSLog(@"______NAO TENS NET___________");
            [self alertaFalhaNET];
        }
        if (error.code == -1011) {
            NSLog(@"______NAO ENCONTROU A CIDADE___________");
            [self alertaCidadeNaoEncontrada];
        }
        
        return;
    }
    
    _forecast = result[@"list"];
    
    [self.collectionProx24H reloadData];
   
}];

}

#pragma mark Testa NET
-(void)testaLigacaoNet{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if(status == NotReachable)
    {
        NSLog(@"_____SEM NET___");
        //No internet
    }
    else if (status == ReachableViaWiFi)
    {
        //WiFi
        NSLog(@"_____WIFI ON___");
    }
    else if (status == ReachableViaWWAN) 
    {
        //3G
        NSLog(@"___________3G___");
    }
}

#pragma mark - collection View Data Source

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section;
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_forecast forKey:@"ultimaPrevCollection"];
    [defaults synchronize];
    if (_forecast.count>5) {
        return 9;
    } else {
        return _forecast.count;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"protoCell" forIndexPath:indexPath];
    
    UILabel *label1 = [cell viewWithTag:1];
    UILabel *label2 = [cell viewWithTag:2];
    UIImageView *iconIV = (UIImageView *)[cell viewWithTag:100];
    
    NSDictionary *forecastData = [_forecast objectAtIndex:indexPath.row];
    
    NSString *preparaDataParaCelula = [NSString stringWithFormat:@"%@", [_forecast objectAtIndex:indexPath.row][@"dt"] ];
    
    NSArray *dataSeparadaCelula = [preparaDataParaCelula componentsSeparatedByString:@" "];
    
    NSString *dataDoArrayCelula = dataSeparadaCelula[0];
    NSString *horaDoArrayCelula = dataSeparadaCelula[1];
    //NSLog(@"DATA_>_>_> %@ HORA_>_>_>%@", dataDoArrayCelula, horaDoArrayCelula);
    
    dataFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *dataParaCelula = [dataFormatter dateFromString:dataDoArrayCelula];
    //NSLog(@"DATA PARA CELULA_>_>_> %@", dataParaCelula);
    
    dataFormatter.dateFormat = @"EEE";
    NSString *diaCelula = [[dataFormatter stringFromDate:dataParaCelula] capitalizedString];
    
    NSString *horaFormatada = [horaDoArrayCelula substringToIndex:5];
    NSString *dataHoraCelula = [NSString stringWithFormat:@"%@ %@", diaCelula, horaFormatada];
   
    label1.text = [NSString stringWithFormat:@"%.1f℃",
                   [forecastData[@"main"][@"temp"] floatValue]
                   ];
    
    label2.text = dataHoraCelula;
    
    NSString *iconPrev = [NSString stringWithFormat:@"%@", forecastData[@"weather"][0][@"icon"]];
    
    // mostra icon AFNETWORKING
    NSString *enderecoApiIcon = @"http://openweathermap.org/img/w/";
    NSMutableString *enderecoFinalIcon = [NSMutableString stringWithFormat:@"%@%@.png", enderecoApiIcon, iconPrev];
    
    [iconIV setImageWithURL:[NSURL URLWithString:enderecoFinalIcon]];
    
    return cell;
}

#pragma mark Alerta Inicial
-(void)alertaInicial{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Localização actual" message:@"Gostaria de usar a sua localização actual?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *localActual = [UIAlertAction actionWithTitle:@"Usar Localização Actual" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self mostraDadosGPSnoArranque];
        
        arranque = YES;
        
    }];
    
    UIAlertAction *agoraNao = [UIAlertAction actionWithTitle:@"Agora não" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        arranque = YES;
        [self mostraUltimas];
        NSLog(@"AGORA NAO");
    }];
    
    
    [alert addAction:localActual];
    [alert addAction:agoraNao];
    
    
    [self presentViewController:alert animated:YES completion:nil];

}

#pragma mark Alerta Falha NET
-(void)alertaFalhaNET{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ligação á Internet Inexistente" message:@"Precisamos de uma ligação á Internet para te mostrar resultados actualizados.\nVerifica a tua ligação e volta a tentar" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *mostrarUltima = [UIAlertAction actionWithTitle:@"Mostrar dados desactualizados" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self mostraUltimas];
        NSLog(@"dados desactualizados");
    }];
    
    UIAlertAction *tentarNovamente = [UIAlertAction actionWithTitle:@"Tentar novamente" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self mostraDados];
        NSLog(@"Tentar novamente");
    }];
    
    
    [alert addAction:mostrarUltima];
    [alert addAction:tentarNovamente];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark Alerta Cidade Nao Encontrada
-(void)alertaCidadeNaoEncontrada{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Oops!!!" message:@"Não conseguimos encontrar a cidade que procuras!!!\nVerifica se escreveste correctamente e volta a tentar." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        self.tabBarController.selectedIndex = 3;

    }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark Mostra Ultimas
-(void)mostraUltimas{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUltimas = [defaults objectForKey:@"ultimaCurrent"];
    NSDate *dateToday = [NSDate date];
    NSLog(@"DICIONARIO ULTIMAS:%@", dictUltimas);
    //[defaults synchronize];
    
    [[[[self.tabBarController tabBar]items]objectAtIndex:1]setEnabled:NO];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.cityName.text = [NSString stringWithFormat:@"%@, %@",
                         dictUltimas[@"name"],
                         dictUltimas[@"sys"][@"country"]
                         ];
    
    // mostra temp actual
    self.currentTemp.text = [NSString stringWithFormat:@"%.1fºC",
                             [dictUltimas[@"main"][@"temp"] floatValue] ];
    
    // mostra humidade
    self.humidadeLabel.text = [NSString stringWithFormat:@"%@%%",
                               dictUltimas[@"main"][@"humidity"]];
    
    // mostra velocidade vento
    self.velocVentoLabel.text = [NSString stringWithFormat:@"%@m/s",
                                 dictUltimas[@"wind"][@"speed"]];
    
    // mostra nascer sol
    NSString *horaNascerSol = [NSString stringWithFormat:@"%@",dictUltimas[@"sys"][@"sunrise"]];
    NSRange rangeHora = NSMakeRange(11, 5);
    NSString *horaNascerSolFormatada = [horaNascerSol substringWithRange:rangeHora];
    
    //NSLog(@"_____HORA NASCER SOL: %@________",horaNascerSol);
    //NSLog(@"_____HORA NASCER SOL FORMATADA:%@________",horaNascerSolFormatada);
    
    self.horaNascerSolLabel.text = horaNascerSolFormatada;
    
    // mostra por sol
    NSString *horaPorSol = [NSString stringWithFormat:@"%@",dictUltimas[@"sys"][@"sunset"]];
    NSString *horaPorSolFormatada = [horaPorSol substringWithRange:rangeHora];
    
    //NSLog(@"_____HORA POR SOL: %@________",horaPorSol);
    //NSLog(@"_____HORA POR SOL FORMATADA:%@________",horaPorSolFormatada);
    
    self.horaPorSolLabel.text = horaPorSolFormatada;
    
    // mostra temp min
    self.tempMin.text = [NSString stringWithFormat:@"%.1fºC",
                         [dictUltimas[@"main"][@"temp_min"] floatValue] ];
    
    // mostra temp max
    self.tempMax.text = [NSString stringWithFormat:@"%.1fºC",
                         [dictUltimas[@"main"][@"temp_max"] floatValue] ];
    
    // mostra ultima actalização
    //NSString *dataArray = [_dateFormatter stringFromDate:dictUltimas[@"dt"]];
    //NSLog(@"___dataArray ultimas:%@", dataArray);

        self.timeStamp.text =  [_dateFormatter stringFromDate:dictUltimas[@"dt"]];

    
    // mostra condições weather
    self.weather.text = dictUltimas[@"weather"][0][@"description"];
    NSString *icon = [NSString stringWithFormat:@"%@", dictUltimas[@"weather"][0][@"icon"]];
    //NSLog(@"%@", icon);
    
    // mostra icon assincrono
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *enderecoApiIcon = @"http://openweathermap.org/img/w/";
        NSMutableString *enderecoFinalIcon = [NSMutableString stringWithFormat:@"%@%@.png", enderecoApiIcon, icon];
        NSURL *url = [NSURL URLWithString:enderecoFinalIcon];
        NSData *imgData = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.iconImageView.image = [UIImage imageWithData:imgData];
        });
        
    });


}

@end
