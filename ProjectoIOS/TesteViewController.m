//
//  TesteViewController.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 03/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import "TesteViewController.h"
#import "CityInfo+CoreDataClass.h"
#import "AppDelegate.h"
//#import "LocaisFavoritosDataSource.h"
#import "Singleton.h"


@interface TesteViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *pesquisaTextFiled;
@property (weak, nonatomic) IBOutlet UITableView *tabela;
@property (weak, nonatomic) IBOutlet UILabel *ultimaLabel;
@property (weak, nonatomic) IBOutlet UITableView *locaisTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnEditar;

@end

@implementation TesteViewController
{
    NSArray<CityInfo *> *_cidades;
    NSMutableArray<CityInfo *> *_locaisFavoritos;
    NSMutableArray *savedArray;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _locaisFavoritos = [[NSMutableArray alloc]init];
    
    self.locaisTableView.separatorStyle = UITableViewCellEditingStyleNone;
    [self.tabela setHidden:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *temp = [defaults objectForKey:@"osMeusLocaisFavoritos"];
    Singleton *single = [Singleton sharedInstance];
    if (temp) {
        single.novosLocais = [NSMutableArray arrayWithArray:temp];
    } else {
        [single.novosLocais removeAllObjects];
    }
    
    [_ultimaLabel setHidden:YES];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *dictUltimas = [defaults objectForKey:@"ultimaCurrent"];
    NSLog(@"DICIONARIO ULTIMAS:%@", dictUltimas);
    
    
    _ultimaLabel.text = [NSString stringWithFormat:@"%@, %@",
                         dictUltimas[@"name"],
                         dictUltimas[@"sys"][@"country"]
                         ];
    
    self.pesquisaTextFiled.text = @"";
    [self.pesquisaTextFiled resignFirstResponder];
    [self.tabela setHidden:YES];
    [self.locaisTableView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated{
    Singleton *single = [Singleton sharedInstance];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:single.novosLocais forKey:@"osMeusLocaisFavoritos"];
    [defaults synchronize];
    
}


- (IBAction)filtraCidades:(id)sender {
    NSLog(@"ESTOU A EDITAR");
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSFetchRequest *cidadesRequest = [CityInfo fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS [cd]%@", self.pesquisaTextFiled.text];
    
    cidadesRequest.predicate = predicate;
   _cidades  = [context executeFetchRequest:cidadesRequest error:nil];
    
    [self.tabela reloadData];
    [self.tabela setHidden:NO];
    if ([self.pesquisaTextFiled.text isEqualToString:@""]) {
            [self.tabela setHidden:YES];
    }
    if (_cidades.count == 0) {
        [self.tabela setHidden:YES];
    }
}

- (IBAction)clickedEditar:(id)sender {

    if ([self.locaisTableView isEditing]) {
        [self.locaisTableView setEditing:NO animated:YES];
    } else{
        [self.locaisTableView setEditing:YES animated:YES];
    }
    
    [self.locaisTableView reloadData];
}
- (IBAction)clickedAdicionar:(id)sender {
    [self performSegueWithIdentifier:@"OpcoesToNovo" sender:nil];
}


#pragma mark Table View DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 0) {
        return _cidades.count;
    } else if (tableView.tag == 2) {
        Singleton *single = [Singleton sharedInstance];
        NSLog(@"____NOVOS LOCAIS COUNT:%ld_____", (unsigned long)single.novosLocais.count);
        NSLog(@"____ARRAY NOVOS LOCAIS:%@______", single.novosLocais);
        if (single.novosLocais.count ==0) {
            _btnEditar.enabled = NO;
        } else{
            _btnEditar.enabled = YES;
        }
        return single.novosLocais.count;
        
    } else{
        return 0;
    }

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell" forIndexPath:indexPath    ];
    
    if (tableView.tag == 0) {
        CityInfo *ci = _cidades[indexPath.row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@, %@", ci.name, ci.country];
    } else if (tableView.tag == 2){
        Singleton *single = [Singleton sharedInstance];
        
        //ordena Array
        [single.novosLocais sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];

        
        cell.textLabel.text = [NSString stringWithFormat:@"%@", single.novosLocais[indexPath.row]];
        cell.layer.borderWidth = 1;
        cell.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.contentView.layer.cornerRadius = 10;
        cell.contentView.layer.masksToBounds = YES;
        
    }
    
    
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView.tag ==0) {
        CityInfo *ci = _cidades[indexPath.row];
        
//        NSLog(@"______CIDADE ID:%d_________", ci.cityId);
//        NSLog(@"______CIDADE LAT:%@_________", ci.lat);
//        NSLog(@"______CIDADE LON:%@_________", ci.lon);
        
        NSString *idCidade = [NSString stringWithFormat:@"%d", ci.cityId];
        NSString *latCidade = [NSString stringWithFormat:@"%@", ci.lat];
        NSString *lonCidade = [NSString stringWithFormat:@"%@", ci.lon];
        //NSString *cityName = [NSString stringWithFormat:@"%@", ci.name];
        //NSString *country = [NSString stringWithFormat:@"%@", ci.country];
        
        [[NSNotificationCenter defaultCenter] postNotificationName: @"idCidade" object: idCidade];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"latCidade" object: latCidade];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"lonCidade" object: lonCidade];
        
        self.tabBarController.selectedIndex = 0;
        
        [self.locaisTableView reloadData];

    } else if(tableView.tag == 2){
        Singleton *single = [Singleton sharedInstance];
        NSString *cidadeClicada = single.novosLocais[indexPath.row];
        NSArray *cidadeSeparada = [cidadeClicada componentsSeparatedByString:@", "];
        NSString *cidadeAEnviar = [NSString stringWithFormat:@"%@", cidadeSeparada[0]];
        
        [self formataPalavraPesquisaLivre:cidadeAEnviar];
        
       // [[NSNotificationCenter defaultCenter] postNotificationName: @"CidadeFavorita" object: cidadeAEnviar];
        self.tabBarController.selectedIndex = 0;
        
    
    }
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView.tag == 2) {
        Singleton *single = [Singleton sharedInstance];
        [single.novosLocais removeObjectAtIndex:indexPath.row];
        
    [self.locaisTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [self.tabela setHidden:YES];
}



- (IBAction)didEnd:(id)sender {
    
    NSString *pesquisaLivre = _pesquisaTextFiled.text;
    
    [self formataPalavraPesquisaLivre:pesquisaLivre];
    
    if (pesquisaLivre.length >2) {
    self.tabBarController.selectedIndex = 0;
    }
    [self.view endEditing:YES];
    
}

-(void)formataPalavraPesquisaLivre:(NSString *)palavraEscrita{
    //substitui acentos
    NSData *dataPesquisa = [palavraEscrita dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //passa a minusculas
    NSString *newStr = [[NSString alloc] initWithData:dataPesquisa encoding:NSASCIIStringEncoding].lowercaseString;
    //remove carateres especiais
    NSString* strNoCar = [[newStr componentsSeparatedByCharactersInSet:[[NSCharacterSet letterCharacterSet] invertedSet]] componentsJoinedByString:@" "];
    
    //remove espaços a mais e substitui por %20
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@" +" options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSString *pesquisaFormatada = [regex stringByReplacingMatchesInString:strNoCar options:0 range:NSMakeRange(0, [strNoCar length]) withTemplate:@"%20"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName: @"pesquisaLivre" object: pesquisaFormatada];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
