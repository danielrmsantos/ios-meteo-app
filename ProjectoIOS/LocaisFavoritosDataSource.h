//
//  LocaisFavoritosDataSource.h
//  ProjectoIOS
//
//  Created by Daniel Santos on 09/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocaisFavoritos.h"

@interface LocaisFavoritosDataSource : NSObject

@property (strong, nonatomic, readonly) NSMutableArray<LocaisFavoritos *> *locaisFavoritosArr;

+ (instancetype)defaultDataSource;

@end
