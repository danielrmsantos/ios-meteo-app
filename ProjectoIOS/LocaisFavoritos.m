//
//  LocaisFavoritos.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 09/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import "LocaisFavoritos.h"

@implementation LocaisFavoritos

- (id)initWithCoder:(NSCoder *)decoder {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.cidade = [decoder decodeObjectForKey:@"cidade"];
    self.idCidade = [decoder decodeObjectForKey:@"idCidade"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.cidade forKey:@"cidade"];
    [encoder encodeObject:self.idCidade forKey:@"idCidade"];
}

- (instancetype)initWithNome:(NSString *)cidade idCidade:(NSString *)idCidade country:(NSString *)country
{
    self = [super init];
    if (self) {
        _cidade = cidade;
        _idCidade = idCidade;
    }
    return self;
}

+ (instancetype)locaisFavoritosWithNome:(NSString *)cidade idCidade:(NSString *)idCidade country:(NSString *)country{
    
    return [[LocaisFavoritos alloc] initWithNome:cidade idCidade:idCidade country:(NSString *)country];
    
}

@end
