//
//  TesteViewController.h
//  ProjectoIOS
//
//  Created by Daniel Santos on 03/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LocaisFavoritos;

@interface TesteViewController : UIViewController

@property (strong, nonatomic) LocaisFavoritos *localFavorito;

@end
