//
//  CityCreator.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 03/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import "CityCreator.h"
#import "CityInfo+CoreDataClass.h"
#import "AppDelegate.h"

@implementation CityCreator

+ (void)createCities {

    // 0 - Verificar se já foi feito
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL feito = [defaults boolForKey:@"feito"];
    
    if (feito) {
        return;
    }
    
    NSLog(@"WEEEEE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    
    // 1 - Ler o ficheiro
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *url = [mainBundle URLForResource:@"cidades_teste" withExtension:@"txt"];
    
    NSString *string = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    
    // 2 - Criar Um Array
    NSArray *array = [string componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    
    // 3 - Colocar o array em CoreData
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    
    
    for (int i = 0; i < array.count; i++) {
        NSString *linha = array[i];
        
        NSData *dataLinha = [linha dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:dataLinha options:kNilOptions error:nil];
        
        CityInfo *ci = [NSEntityDescription insertNewObjectForEntityForName:@"CityInfo" inManagedObjectContext:context];
        
        ci.cityId = ((NSNumber *)dict[@"_id"]).intValue;
        ci.lat = ((NSNumber *)dict[@"coord"][@"lat"]).description;
        ci.lon = ((NSNumber *)dict[@"coord"][@"lon"]).description;
        ci.country = dict[@"country"];
        ci.name = dict[@"name"];
        
    }
    
    [appDelegate saveContext];
    
    // 4 - Marcar como feito

    [defaults setBool:YES forKey:@"feito"];
    [defaults synchronize];
}

@end
