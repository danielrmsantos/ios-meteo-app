//
//  NovoViewController.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 10/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import "NovoViewController.h"
#import "Singleton.h"
#import "CityInfo+CoreDataClass.h"
#import "AppDelegate.h"
#import <MBProgressHUD.h>
#import "ViewController.h"

@interface NovoViewController ()
@property (weak, nonatomic) IBOutlet UITextField *cidadeTF;
@property (weak, nonatomic) IBOutlet UITextField *latitudeTF;
@property (weak, nonatomic) IBOutlet UITextField *longitudeTF;
@property (weak, nonatomic) IBOutlet UITableView *tabela;

@property (strong, nonatomic) CLLocationManager *locationManager;


@end

@implementation NovoViewController
{
    NSArray<CityInfo *> *_cidades;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    //self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.tabela setHidden:YES];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [self.cidadeTF becomeFirstResponder];
}


- (IBAction)clickedVoltar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)clickedGuardar:(id)sender {
    Singleton *single = [Singleton sharedInstance];
    NSString *novaCidade = _cidadeTF.text;
    //single.novosLocais = [[NSMutableArray alloc]init];
    if (![single.novosLocais containsObject:novaCidade]) {
        [single.novosLocais addObject:novaCidade];
    } else {
        [self alertaCidadeDuplicada];
        return;
    }
        
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
    
}

- (IBAction)filtraCidades:(id)sender {
    NSLog(@"ESTOU A EDITAR");
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    NSFetchRequest *cidadesRequest = [CityInfo fetchRequest];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS [cd]%@", self.cidadeTF.text];
    
    cidadesRequest.predicate = predicate;
    _cidades  = [context executeFetchRequest:cidadesRequest error:nil];
    
    
    [self.tabela reloadData];
    [self.tabela setHidden:NO];
    if ([self.cidadeTF.text isEqualToString:@""]) {
        [self.tabela setHidden:YES];
    }
    if (_cidades.count == 0) {
        [self.tabela setHidden:YES];
    }
}
    
    // Location Manager Delegate Methods
    - (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
    {
        //NSLog(@"AS minha coord %@", [locations lastObject]);
        CLLocation *minhaLocation = locations[0];
        NSLog(@"AS minha latitude %f", minhaLocation.coordinate.latitude);
        NSLog(@"AS minha lONGITUDE %f", minhaLocation.coordinate.longitude);

        [_locationManager stopUpdatingLocation];
        
        //NSString *locationString = locations[0];
        //NSLog(@"AS minha location string %@________", locationString);
        
        // Reverse Geocoding
        NSLog(@"Resolving the Address");
        [geocoder reverseGeocodeLocation:minhaLocation completionHandler:^(NSArray *placemarks, NSError *error) {
            NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
            if (error == nil && [placemarks count] > 0) {
                placemark = [placemarks lastObject];
                _cidadeTF.text = [NSString stringWithFormat:@"%@\n%@",
                                     placemark.locality,
                                     placemark.country];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            } else {
                NSLog(@"ISTO TA COM ERROS %@", error.debugDescription);
            }
        } ];
    }

- (IBAction)clickedLocalActual:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self.locationManager startUpdatingLocation];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [self.tabela setHidden:YES];
}

#pragma mark Table View DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _cidades.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell" forIndexPath:indexPath    ];
    
    CityInfo *ci = _cidades[indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@, %@", ci.name, ci.country];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
        CityInfo *ci = _cidades[indexPath.row];
    
        //NSString *idCidade = [NSString stringWithFormat:@"%d", ci.cityId];
        //NSString *latCidade = [NSString stringWithFormat:@"%@", ci.lat];
        //NSString *lonCidade = [NSString stringWithFormat:@"%@", ci.lon];
        NSString *cityName = [NSString stringWithFormat:@"%@", ci.name];
        NSString *country = [NSString stringWithFormat:@"%@", ci.country];
        
    _cidadeTF.text = [NSString stringWithFormat:@"%@, %@", cityName, country];
    [self.tabela setHidden:YES];
}

#pragma mark Alerta Cidade Duplicada
-(void)alertaCidadeDuplicada{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Oops!!!" message:@"Já adicionaste esta cidade aos favoritos!" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        
    }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
