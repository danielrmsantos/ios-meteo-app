//
//  NovoViewController.h
//  ProjectoIOS
//
//  Created by Daniel Santos on 10/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface NovoViewController : UIViewController <CLLocationManagerDelegate>

@end
