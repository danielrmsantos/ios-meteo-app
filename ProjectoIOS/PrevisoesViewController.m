//
//  PrevisoesViewController.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 07/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import "PrevisoesViewController.h"
#import <OWMWeatherAPI.h>
#import <MBProgressHUD.h>
#import <UIImageView+AFNetworking.h>
#import "DetailViewController.h"
#import "Singleton.h"

@interface PrevisoesViewController ()

@property (weak, nonatomic) IBOutlet UITableView *previsoesTableViewControl;
@property (weak, nonatomic) IBOutlet UINavigationItem *navBar;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedDias;

@end

@implementation PrevisoesViewController
{
    OWMWeatherAPI *_weatherAPI;
    NSString *apiKey;
    NSArray *_forecast;
    NSDateFormatter *_dateFormatter;
    NSDateFormatter *dataFormatter;

    
    NSString *cityId;
    NSString *cityIdSingleTon;
    int downloadCount;
    int segSelected;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    apiKey = @"PUT YOUR OPEN WEATHER MAP API KEY HERE";

    segSelected = 3;
    
    dataFormatter = [[NSDateFormatter alloc]init];
    self.previsoesTableViewControl.separatorStyle = UITableViewCellEditingStyleNone;
    
}

-(void)viewWillAppear:(BOOL)animated{
   
    Singleton *single = [Singleton sharedInstance];
    cityIdSingleTon = [NSString stringWithFormat:@"%@", single.idCidade];
    NSLog(@"______ID CIDADDE SINGLETON:%@________",single.idCidade);
}
-(void)viewDidAppear:(BOOL)animated{
   
    [self iniciaPrevisoes];
}

-(void)iniciaPrevisoes {
    NSString *modeloData = @"H:m yyyyMMdd";
    NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:modeloData options:0 locale:[NSLocale systemLocale] ];
    _dateFormatter = [[NSDateFormatter alloc]init];
    [_dateFormatter setDateFormat:dateFormat];
    _forecast = @[];
    _weatherAPI = [[OWMWeatherAPI alloc]initWithAPIKey:apiKey];
    
    [_weatherAPI setLangWithPreferedLanguage];
    [_weatherAPI setTemperatureFormat:kOWMTempCelcius];
    
    [_weatherAPI dailyForecastWeatherByCityId:cityIdSingleTon withCount:segSelected andCallback:^(NSError *error, NSDictionary *result) {
        [_weatherAPI setLangWithPreferedLanguage];
        
        if (error) {
            // Handle the error
            if (error.code == -1009) {
                NSLog(@"______NAO TENS NET___________");
                [self alertaFalhaNET];
            }
            if (error.code == -1011) {
                NSLog(@"______NAO ENCONTROU A CIDADE___________");
                [self alertaCidadeNaoEncontrada];
            }
            
            NSLog(@"--ERRO POR COORDENADAS dailyForecastWeatherByCoordinate MOSTRADADOS:%@, CODIGO ERRO:%ld", error, (long)error.code);
            return;
        }
        
        _forecast = result[@"list"];
        NSLog(@"____RESULT LIST:%@_________", result);
        [self.previsoesTableViewControl reloadData];
    }];
}
- (IBAction)clickedSel:(id)sender {
    
    if (_segmentedDias.selectedSegmentIndex == 0) {
        segSelected = 3;
        self.navigationItem.title = @"Previsões para os próximos 3 Dias";
        [self iniciaPrevisoes];
        NSLog(@"______O MEU SEGMENTED VALUE:%d__________", segSelected);
    } else if (_segmentedDias.selectedSegmentIndex == 1) {
        segSelected = 5;
        self.navigationItem.title = @"Previsões para os próximos 5 Dias";
        [self iniciaPrevisoes];
        NSLog(@"______O MEU SEGMENTED VALUE:%d__________", segSelected);
    } else if (_segmentedDias.selectedSegmentIndex == 2) {
        segSelected = 10;
        self.navigationItem.title = @"Previsões para os próximos 10 Dias";
        [self iniciaPrevisoes];
        NSLog(@"______O MEU SEGMENTED VALUE:%d__________", segSelected);
    }
}


#pragma mark TableView Datasource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _forecast.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell" forIndexPath:indexPath    ];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"protoCell"];
    }
    UILabel *label1 = [cell viewWithTag:1];
    UILabel *label2 = [cell viewWithTag:2];
    UIImageView *iconPrevImageView = (UIImageView *)[cell viewWithTag:100];
    
    _forecastData = [_forecast objectAtIndex:indexPath.row];
    //NSLog(@"-----FORESCAST:%@", _forecastData);
    label1.text = [[NSString stringWithFormat:@"%.1f℃ - %@",
                   [_forecastData[@"temp"][@"day"] floatValue],
                   _forecastData[@"weather"][0][@"description"]
                   ]capitalizedString];
    
    NSString *preparaDataParaCelula = [NSString stringWithFormat:@"%@",_forecastData[@"dt"] ];
    
    NSArray *dataSeparadaCelula = [preparaDataParaCelula componentsSeparatedByString:@" "];
    
    NSString *dataDoArrayCelula = dataSeparadaCelula[0];
    //NSString *horaDoArrayCelula = dataSeparadaCelula[1];
    //NSLog(@"DATA_>_>_> %@ HORA_>_>_>%@", dataDoArrayCelula, horaDoArrayCelula);
    
    dataFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *dataParaCelula = [dataFormatter dateFromString:dataDoArrayCelula];
    //NSLog(@"DATA PARA CELULA_>_>_> %@", dataParaCelula);
    
    dataFormatter.dateFormat = @"dd/MMM";
    NSString *dataDiaMes = [dataFormatter stringFromDate:dataParaCelula];
    //NSLog(@"______DATA TESTES:%@__________", dataDiaMes);
    
    dataFormatter.dateFormat = @"EEEE";
    NSString *diaCelula = [[dataFormatter stringFromDate:dataParaCelula] capitalizedString];
    
    //NSString *horaFormatada = [horaDoArrayCelula substringToIndex:5];
    NSString *dataFormCelula = [NSString stringWithFormat:@"%@ - %@", diaCelula, dataDiaMes];
    
    label2.text = dataFormCelula;
    
    //mostra icon AFNETWORKING
    NSString *iconPrev = [NSString stringWithFormat:@"%@", _forecastData[@"weather"][0][@"icon"]];
    NSString *enderecoApiIcon = @"http://openweathermap.org/img/w/";
    NSMutableString *enderecoFinalIcon = [NSMutableString stringWithFormat:@"%@%@.png", enderecoApiIcon, iconPrev];
    
    [iconPrevImageView setImageWithURL:[NSURL URLWithString:enderecoFinalIcon]];
    
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.contentView.layer.cornerRadius = 10;
    cell.contentView.layer.masksToBounds = YES;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    _forecastDataAEnviar = [_forecast objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"PrevisoesToDetail" sender:nil];
    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"PrevisoesToDetail"]) {
        DetailViewController *dvc = segue.destinationViewController;
        dvc.forecastDataRecebida = _forecastDataAEnviar;
    }

}

#pragma mark Alerta Falha NET
-(void)alertaFalhaNET{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ligação á Internet Inexistente" message:@"Precisamos de uma ligação á Internet para te mostrar resultados actualizados.\nVerifica a tua ligação e volta a tentar" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *mostrarUltima = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        NSLog(@"dados desactualizados");
    }];
    
    UIAlertAction *tentarNovamente = [UIAlertAction actionWithTitle:@"Tentar novamente" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self iniciaPrevisoes];
        NSLog(@"Tentar novamente");
    }];
    
    
    [alert addAction:mostrarUltima];
    [alert addAction:tentarNovamente];
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

#pragma mark Alerta Cidade Nao Encontrada
-(void)alertaCidadeNaoEncontrada{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Oops!!!" message:@"Não conseguimos encontrar a cidade que procuras!!!\nVerifica se escreveste correctamente e volta a tentar." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        self.tabBarController.selectedIndex = 3;
        
    }];
    
    [alert addAction:ok];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}


@end
