//
//  main.m
//  ProjectoIOS
//
//  Created by Daniel Santos on 26/11/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
