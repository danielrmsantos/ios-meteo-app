//
//  Singleton.h
//  ProjectoIOS
//
//  Created by Daniel Santos on 08/12/16.
//  Copyright © 2016 Daniel Santos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject

@property (strong, nonatomic) NSString *idCidade;
@property (strong, nonatomic) NSMutableArray<NSString *> *novosLocais;

+ (instancetype)sharedInstance;

@end
